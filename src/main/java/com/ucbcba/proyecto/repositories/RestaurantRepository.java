package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {
}
